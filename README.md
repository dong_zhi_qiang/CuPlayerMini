#CuPlayerMini
官方网址： http://www.cuplayer.com/
这是第三版和第四版的源码
这里是免费版本的，官方有收费版本的

酷播迷你CuPlayerMiniV4.1版(永久免费版)

酷播迷你CuPlayerMiniV4.1  重要功能更新说明( 2013-7-5更新 )：
1. 重写播放器基础架构； 
2. 修改播放器双击全屏功能； 
3. 修改显示视频略缩图功能；
4. 支持自定义用户logo功能； 
5. 支持flv,mp4,mov,f4v,3gp等视频格式； 
6. 优化视频比例问题；
7. 新增配置文件功能，此功能将实现与asp,php,jsp,aspx程序更完美的结合；
8. 支持双重参数功能，可适合普通用户和程序人员使用； 
9. 更加人性化的播放体验；
10. 支持FlashVars调用html代码中的参数； 
11. 支持自动播放/点击播放；
12. 显示播放进度条，带预加载，支持拖动播放进度条； 
13. 显示视频总时长和当前播放进度的位置时间；
14. 支持音量控制； 
15. 支持列表连播参数getNext，调用时直接在html中添加js的getNext函数即可；
16. 可以与动易、织梦、帝国等各类CMS系统结合，实现与此类CMS系统后台的融合

酷播迷你CuPlayerMiniV4.0  重要功能更新说明(2013-6-16更新)：
1. 重写播放器基础架构； 
2. 修改播放器双击全屏功能； 
3. 修改显示视频略缩图功能；
4. 支持flv,mp4,mov,f4v,3gp等视频格式； 
5. 新增配置文件功能，此功能将实现与asp,php,jsp,aspx程序更完美的结合；
6. 支持双重参数功能； 
7. 更加人性化的播放体验；